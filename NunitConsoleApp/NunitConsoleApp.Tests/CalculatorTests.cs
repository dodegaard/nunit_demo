﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace NunitConsoleApp.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void MultiplyIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply( 9, 3 );

            Assert.AreEqual(27,result);
        }

        [TestCase(12, 3, 36)]
        [TestCase(12, 2, 24)]
        [TestCase(12, 4, 48)]
        public void MultiplyGoodValues(int d1, int d2, int result)
        {
            var calc = new Calculator();
            var calcResult = calc.Multiply(d1, d2);
        
            Assert.AreEqual(calcResult,result);

        }


    }
}
